define CROSSPLATFORM_TEST
#ifdef CROSSPLATFORM_TEST
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../crossplatform/pd.h"
#include "../crossplatform/logprintf.h"

int main()
{
    unsigned int tickStart = GetTickCount();
    Sleep(1);
    XConfigLog(X_LOG_VERBOSE, X_LOG_TYPE_CONSOLE, NULL);
    XLogPrintf(X_LOG_DEBUG, "crossplatform", "timeset=%u\r\n", GetTickCount() - tickStart);
    XLIB_LOCK_T myLock;
    XLIB_INITLOCK(myLock);
    XLIB_LOCKIN(myLock);
    XLIB_LOCKOUT(myLock);
    XLIB_FREELOCK(myLock);
    XLogPrintf(X_LOG_INFO, "crossplatform", "finished!\r\n");
    return 0;
}
#endif // CROSSPLATFORM_TEST
