#include "pd.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#ifndef _WIN32
#include <unistd.h>
#include <sys/time.h>
#include <time.h>

void Sleep(int ms)
{
	usleep(ms*1000);
}


unsigned int GetTickCount()
{
	struct timeval tv;
	struct timezone tz;

	memset(&tv, 0 ,sizeof(tv));
	gettimeofday(&tv, &tz);

	return (tv.tv_sec * 1000 + tv.tv_usec / 1000);
}
#endif // _WIN32
