#ifndef PD_H_INCLUDED
#define PD_H_INCLUDED
#include <stdio.h>

//needed -lpthread

#ifdef _WIN32
#include <Windows.h>
#define XLIB_INITLOCK(lock)	InitializeCriticalSection(&lock)
#define XLIB_FREELOCK(lock) DeleteCriticalSection(&lock)
#define XLIB_LOCKIN(lock) EnterCriticalSection(&lock)
#define XLIB_LOCKOUT(lock)	LeaveCriticalSection(&lock)
#define XLIB_LOCK_T CRITICAL_SECTION
#else
#include <pthread.h>
#define XLIB_INITLOCK(lock)	pthread_mutex_init(&lock, NULL)
#define XLIB_FREELOCK(lock) pthread_mutex_destroy(&lock)
#define XLIB_LOCKIN(lock) pthread_mutex_lock(&lock)
#define XLIB_LOCKOUT(lock)	pthread_mutex_unlock(&lock)
#define XLIB_LOCK_T pthread_mutex_t
#endif

#ifndef _WIN32
void Sleep(int ms);

unsigned int GetTickCount();
#endif

/*****************************************
************SDK 版本生成规则*****************
*****************************************/
//x.x.x.x\tDATE_TIME
//format("%s\t%s %s", version, __DATE__, __TIME__);


#endif // PD_H_INCLUDED
