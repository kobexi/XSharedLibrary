#ifndef LOGPRINT_H
#define LOGPRINT_H

typedef enum X_LOG_LEVEL {

   X_LOG_VERBOSE = 2,

   X_LOG_DEBUG = 3,

   X_LOG_INFO = 4,

   X_LOG_WARN = 5,

   X_LOG_ERROR = 6,

   X_LOG_ASSERT = 7,
} X_Log_Level;

typedef enum X_LOG_TYPE{
	X_LOG_TYPE_CONSOLE		=	1,	//console(android logcat)
	X_LOG_TYPE_FILE		=	2,
	X_LOG_TYPE_DBGVIEW		=	3,	//windows only
}X_Log_Type;



void XConfigLog(int nLevel, int nType, char* fileName);

void XLogPrintf(int nLevel, const char* tag, const char* fmt, ...);

#endif

