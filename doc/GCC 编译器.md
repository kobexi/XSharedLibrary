# GCC 编译器

## 过程文件

| 后缀 | 含义               | 描述                                                     |
| ---- | ------------------ | -------------------------------------------------------- |
| .c   | 源文件             | 源代码                                                   |
| .o   | 源文件的目标文件   | 源代码预处理，编译和汇编后的二进制                       |
| .a   | 目标文件的归档文件 | 目标文件的打包，没有链接过程                             |
| .so  | 动态链接库文件     | 由目标文件或归档文件链接而成，没有程序入口，不能独立运行 |
| .out | 编译输出可执行文件 | 由目标文件或归档文件链接而成，有程序入口，可以独立运行   |

## 常用工具

| 工具 | 描述                                                         |
| ---- | ------------------------------------------------------------ |
| gcc  | 通常用于完成预处理、编译、汇编、链接工作 (.c -> .o .so .out) |
| ar   | 通常用于归档目标文件 (.o -> .a)                              |

## 常用方法

1. 生成可执行文件

```
gcc -o app.out  main.c
```

2. 生成动态库文件

```
gcc -fPIC -shared -o libfunc.so  func.c
```

3. 生成目标文件

```
gcc -c fun1.c fun2.c
```

4. 生成静态库文件

```
ar rcs fun.a  fun1.o fun2.o
```

## 常用选项

| 参数        | 描述                               |
| ----------- | ---------------------------------- |
| -I          | 指明头文件的包含路径               |
| -L          | 指明静态库的包含路径               |
| -l          | 指明静态库的名字                   |
| -Wl,-rpath= | 指明运行时可以找到动态链接库的路径 |
| -g          | 编译时产生调试信息                 |
| -Wall       | 生成所有警告信息                   |
| -On         | n=0~3 优化级别，-O3最高            |
| -static     | 禁止使用动态库                     |
| -share      | 尽量使用动态库                     |
| -fPIC       | 产生与位置无关的代码               |
| -c          | 激活预处理，编译和汇编             |
| -o          | 指定目标名称                       |

## 华为GCC安全选项

```
-O3 -fstrict-aliasing -ftrapv -fwrapv -D_FORTIFY_SOURCE=2 -fstack-protector-all -Wl,-z,now,-z,relro -Werror
```

## 第三方库符号隐藏编译方法

应用软件与SDK可能存在共同引用某些第三方库的情况，一旦存在版本差异会导致程序崩溃等问题。所以建议尽可能将引用的第三方库以静态链接的方式编译，并使用如下编译选项将符号隐藏掉。

```
g++ -Wall -g sample.cpp -fvisibility=hidden -fvisibility-inlines=hidden -fPIC -shared -Wl,--exclude-libs=ALL -Wl,--whole-archive libprotobuf.a -Wl,--no-whole-archive -Wl,-Bsymbolic -o libsample.so

说明：
1、-fvisibility=hidden -fvisibility-inlines=hidden 隐藏自有代码符号
2、-Wl,--exclude-libs=ALL 隐藏链接静态库的符号
3、-Wl,--whole-archive  -Wl,--no-whole-archive 这个放在要链接的静态库两边，表示要链接静态库的所有内容
4、-Wl,-Bsymbolic 表示编译出来的so，优先调用其内部的代码，放在应用开源so库加载时，静态库调用到开源so库的代码
5、编译命令中静态库未知放到.o文件后面

代码实现参考：
在要导出的函数前面加上__attribute__((visibility("default")))
仅在实现文件加，头文件不加，避免第三方引用头文件导出这些函数
__attribute__((visibility("default"))) int test_func()
{
	return 0;
}
```

